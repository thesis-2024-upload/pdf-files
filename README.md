# Remark

| No | Student Name | Thesis Topic | 
|:-|:-|:-|
| 1 | VONG TITHTOLA (2023-2024) | Development Multi-Protocol IoT Gateway to Support OpenThread and LoRaWAN Protocol |
| 2 | LOU YOUFUK (2023-2024) | Development of LoRaWAN Tester to identify the Range Coverage |
| 3 | HUN VANNPANHA (2023-2024) | Development power metering device using OpenThread protocol |